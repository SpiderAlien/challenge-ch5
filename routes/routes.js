const { request } = require("express")
const express = require ("express")
const router = express.Router ()
const users = []

router.use((request, response, next) => {
    console.log("route level middleware")
    next()
})

router.get("/home", (request, response) =>{
    response.render("home")
})

router.get("/game", (request, response) =>{
    response.render("game")
})



module.exports = router