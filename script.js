// const playerB = document.getElementById('batu-p');
// const playerK = document.getElementById('kertas-p');
// const playerG = document.getElementById('gunting-p');
// const compB = document.getElementById('rockcom');
// const compK = document.getElementById('papercom');
// const compG = document.getElementById('scissorscom');



// function getPilihanComputer(){
//     var comp = Math.random();
//     if(comp < 0.34) return`rockcom`;
//     if(comp >= 0.34 && comp <0.67) return `papercom`;
//     return `scissorscom`;
// }

// function getHasil(comp, player){
//     if( player == comp ) return 'seri';
//     if( player == 'gunting' ) return ( comp == 'kertas' ) ? 'win' : 'lose';
//     if( player == 'kertas' ) return ( comp == 'gunting' ) ? 'lose' : 'win';
//     if( player == 'batu' ) return ( comp == 'gunting' ) ? 'win' : 'lose';
    
// }

// const pilihan = document.getElementsByClassName('myoption');
// pilihan.forEach(function(pil){
//     pil.addEventListener('click',function(){
//     const pilihanComputer = getPilihanComputer();
//     const pilihanPlayer = pilihan;
//     const hasil = getHasil(pilihanComputer,pilihanPlayer);
//     console.log(pilihan);

//     })
// });




function getPilihanComputer(){
    const comp = Math.random();
    if( comp < 0.34 ) return 'kertas'; 
    if( comp >= 0.34 && comp < 0.67 ) return 'gunting'; 
    return 'batu'; 
}

function getHasil(comp, player){
    if( player == comp ) return 'seri';
    if( player == 'gunting' ) return ( comp == 'kertas' ) ? 'win' : 'lose';
    if( player == 'kertas' ) return ( comp == 'gunting' ) ? 'lose' : 'win';
    if( player == 'batu' ) return ( comp == 'gunting' ) ? 'win' : 'lose';
    
}

function setHasil(hasil){
  if (hasil == 'win') {
    document.querySelector('#playerwin').setAttribute('class','d-block bg-success p-3');
    document.querySelector('#default').setAttribute('class','d-none');
    document.querySelector('#comwin').setAttribute('class','d-none');
    document.querySelector('#seri').setAttribute('class','d-none');
    document.querySelector('#refresh-logo').setAttribute('class','d-block');
  } 
  else if (hasil == 'seri') {
    document.querySelector('#playerwin').setAttribute('class','d-none');
    document.querySelector('#default').setAttribute('class','d-none');
    document.querySelector('#comwin').setAttribute('class','d-none');
    document.querySelector('#seri').setAttribute('class','d-block bg-success p-3');
    document.querySelector('#refresh-logo').setAttribute('class','d-block');
  } 
  else if (hasil == 'lose') {
    document.querySelector('#playerwin').setAttribute('class','d-none');
    document.querySelector('#default').setAttribute('class','d-none');
    document.querySelector('#comwin').setAttribute('class','d-block bg-success p-3');
    document.querySelector('#seri').setAttribute('class','d-none');
    document.querySelector('#refresh-logo').setAttribute('class','d-block');
  }
}

function setComputer(pilihanComputer){
  if (pilihanComputer == 'kertas') {
    document.querySelector('#papercom').setAttribute('class','active-com');
  } 
  else if (pilihanComputer == 'gunting') {
    document.querySelector('#scissorscom').setAttribute('class','active-com');
  } 
  else if (pilihanComputer == 'batu') {
    document.querySelector('#rockcom').setAttribute('class','active-com');
  }
}

function setFreeze(hasil){
  if (hasil != null){
    document.querySelector('#paperplayer').classList.add('no-click');
    document.querySelector('#scissorsplayer').classList.add('no-click');
    document.querySelector('#rockplayer').classList.add('no-click');
  }
}

function refreshButton(){
  hasil = null;
  document.querySelector('#rockcom').setAttribute('class','suitcom');
  document.querySelector('#papercom').setAttribute('class','suitcom');
  document.querySelector('#scissorscom').setAttribute('class','suitcom');
  document.querySelector('#playerwin').setAttribute('class','d-none');
  document.querySelector('#default').setAttribute('class','d-block p-5');
  document.querySelector('#comwin').setAttribute('class','d-none');
  document.querySelector('#seri').setAttribute('class','d-none');
  document.querySelector('#paperplayer').classList.remove('player-choice');
  document.querySelector('#scissorsplayer').classList.remove('player-choice');
  document.querySelector('#rockplayer').classList.remove('player-choice');
  document.querySelector('#papercom').classList.remove('active');
  document.querySelector('#scissorscom').classList.remove('active');
  document.querySelector('#rockcom').classList.remove('active');
  document.querySelector('#refresh-logo').setAttribute('class','d-block');
}

// Get all buttons with class="btn" inside the container
var btnContainer = document.getElementById("asset-player");

// Get all buttons with class="btn" inside the container
// var btns = btnContainer.getElementsByClassName("myoption");

// // Loop through the buttons and add the active class to the current/clicked button
// for (var i = 0; i < btns.length; i++) {
//   btns[i].addEventListener("click", function() {
//     var current = document.getElementsByClassName("active");

//     // If there's no active class
//     if (current.length > 0) { 
//       current[0].className = current[0].className.replace("active", "");
//     }

//     // Add the active class to the current/clicked button
//     this.className += " active";
//   });
// }

function putar(){
}
const pKertas = document.querySelector('#paperplayer');
const pGunting = document.querySelector('#scissorsplayer');
const pBatu = document.querySelector('#rockplayer');



pGunting.addEventListener('click',function(){
    this.classList.add("player-choice")
    const pilihanComputer = getPilihanComputer(); 
    // let pilihanPlayer = 'scissorsplayer';
    let pilihanPlayer = 'gunting';
    const hasil = getHasil(pilihanComputer, pilihanPlayer);

    setHasil(hasil);
    setComputer(pilihanComputer);
    setFreeze(hasil);
    console.log("Pilihan Player: "+pilihanPlayer);
    console.log("Pilihan Computer: "+pilihanComputer);
    console.log("Hasil: "+hasil);
})

pKertas.addEventListener('click',function(){
  this.classList.add("player-choice")
  const pilihanComputer = getPilihanComputer(); 
  // let pilihanPlayer = 'paperplayer';
  let pilihanPlayer = 'kertas';
  const hasil = getHasil(pilihanComputer, pilihanPlayer);
  setHasil(hasil);
  setComputer(pilihanComputer);
  setFreeze(hasil);
  // putar();
  console.log("Pilihan Player: "+pilihanPlayer);
  console.log("Pilihan Computer: "+pilihanComputer);
  console.log("Hasil: "+hasil);
})


pBatu.addEventListener('click',function(){
  this.classList.add("player-choice")
  const pilihanComputer = getPilihanComputer(); 
  // let pilihanPlayer = 'rockplayer';
  let pilihanPlayer = 'batu';
  var hasil = getHasil(pilihanComputer, pilihanPlayer);
  setHasil(hasil);
  setComputer(pilihanComputer);
  setFreeze(hasil);
  console.log("Pilihan Player: "+pilihanPlayer);
  console.log("Pilihan Computer: "+pilihanComputer);
  console.log("Hasil: "+hasil);
})

const pRefresh = document.querySelector('#refresh-logo');
pRefresh.addEventListener('click',function(){
    refreshButton();
    
})


class clickButton {
    constructor(pilihanPlayer,pilihanComputer,hasil){
      this.pilihanComputer = pilihanComputer;
      this.pilihanPlayer = pilihanPlayer;
      this.hasil = hasil;
    }

    
}


